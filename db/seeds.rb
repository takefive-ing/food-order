# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')
5.times do
  name = Faker::Name.name
  User.create(name: name,
              phone: Faker::PhoneNumber.cell_phone,
              address: Faker::Address.street_address,
              email: Faker::Internet.email(name),
              password: '123123123',
              password_confirmation: '123123123'
  )
end
#-----------------------------------------------
# DISH_COUNT = 5
PLACE_COUNT = 10

def create_place
  place_hashes = []
  PLACE_COUNT.times do
    place_hashes.push({
                          name: Faker::Company.name,
                          image: File.new(Rails.root.join("public", "temp", "#{rand(1..7)}.jpg")), #Faker::Avatar.image,
                          description: Faker::Lorem.paragraph,
                      })
  end
  Place.create(place_hashes)
end

place = create_place

def get_random_element(array)
  array[rand(0...array.length)]
end


20.times do
  Dish.create({name: Faker::Commerce.product_name,
               price: Faker::Commerce.price,
               description: Faker::Lorem.paragraph,
               place_id: get_random_element(place).id
              })
end
