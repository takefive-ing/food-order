class RemoveImageFromPlace < ActiveRecord::Migration
  def change
    remove_column :places, :image, :string
  end
end
