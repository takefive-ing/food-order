ActiveAdmin.register User do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  permit_params :name, :phone, :address, :email, :encrypted_password

  index do
    selectable_column
    id_column
    column :name
    column :address
    column :phone
    column :email
  end

  # form do |f|
  #   f.inputs do
  #     f.input :name
  #     f.input :phone
  #     f.input :address
  #     f.input :email
  #     f.input :encrypted_password
  #
  #   end
  # end
end
